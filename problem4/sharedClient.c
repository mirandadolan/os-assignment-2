/*
Author: Miranda Dolan
Date: 10/3/2018

The client program reads a line from `shm-example.txt`, converts everything to
upper case letters, and send it to the server, using SysV shared memory.
It pauses for 1 second, and repeats the above process, until the end of file.
*/

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/shm.h>
#include <stdlib.h>

const int SHM_SIZE = 1024;  // make it a 1K shared memory segment
const char FILE_NAME[] = "shm-example.txt";

int main(int argc, char *argv[]) {
  printf("\nStarting the client! \n");
  key_t key;
  int shmid;
  char *data;
  int n;

  // make the key:
  if ((key = ftok(FILE_NAME, 1)) == -1) {
      perror("ftok");
      exit(1);
  }

  // connect to (and possibly create) the segment:
  if ((shmid = shmget(key, SHM_SIZE, 0644 | IPC_CREAT)) == -1) {
      perror("shmget");
      exit(1);
  }

  // attach to the segment to get a pointer to it:
  data = shmat(shmid, (void *)0, 0);
  if (data == (char *)(-1)) {
      perror("shmat");
      exit(1);
  }

  int MAXCHAR = 1000;
  FILE *fp;
  char str[MAXCHAR];
  char filename[] = "shm-example.txt";

  fp = fopen(filename, "r");

  // read text file line by line
  while (fgets(str, MAXCHAR, fp) != NULL){
    for(int i=0;i<strlen(str);i++){ // convert each character to upper case
        str[i] = toupper(str[i]);
    }
    printf("Sending data. . .\n");
    strncpy(data, str, SHM_SIZE); // send data to server
    sleep(5);
  }
  strncpy(data, "STOP", SHM_SIZE);
  printf("Finished!\n");

  // close client
  fclose(fp);
  return 0;

}
