/*
Author: Miranda Dolan
Date: 10/3/2018

Prints any data received from the client.
*/

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/shm.h>


const int SHM_SIZE = 1024;  // make it a 1K shared memory segment

const char FILE_NAME[] = "shm-example.txt";

int main(int argc, char *argv[]) {

  printf("\nStarting the server. . . \n");

  key_t key;
  int shmid;
  char *data;
  int n;

  // make the key:
  if ((key = ftok(FILE_NAME, 1)) == -1) {
      perror("ftok");
      exit(1);
  }

  // connect to (and possibly create) the segment:
  if ((shmid = shmget(key, SHM_SIZE, 0644 | IPC_CREAT)) == -1) {
      perror("shmget");
      exit(1);
  }

  // attach to the segment to get a pointer to it:
  data = shmat(shmid, (void *)0, 0);
  if (data == (char *)(-1)) {
      perror("shmat");
      exit(1);
  }

  // read first 10 lines of file or until a STOP occurs
  n = 0;
  while ( n < 10 && (strcmp(data, "STOP") != 0) ){
      printf("%s\n", data);
      sleep(5);   /* Sleep 5 seconds) */
      n++;
  }

  // detach from the segment:
  if (shmdt(data) == -1) {
      perror("shmdt");
      exit(1);
  }

  // delete the IPC structure
  shmctl(shmid, IPC_RMID, NULL);
  // close the socket
  close(shmid);
  return 0;

}
