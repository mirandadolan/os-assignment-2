/*
Author: Miranda Dolan
Date: 9/26/2018

The program recursively find all the `.c` files under the current directly.
It sends all the file names it found in an email message to an email address provided
via the command line.  The subject line contains the number of files found.
See README for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <iostream>
#include <array>
#include <cstdio>

using namespace std;

int main(int argc, char *argv[]){

  FILE* r_pipe;
  string email = argv[1]; // command line arg for email
  string find_cmd = ("find . -name \"*.c\" -print"); // command for finding .c files
  array<char, 128> buffer;
  string result;
  int count = 0;

  r_pipe = popen(find_cmd.c_str(), "r"); // pipe to read outputs

  // buffer will hold data from find command and each data will be added to result
  while (fgets(buffer.data(), 128, r_pipe) != NULL) {
    result += buffer.data();
    count += 1; // count of files found
  }

  string email_cmd = ("echo '" + result + "' | " + "mailx -s \"Found " + to_string(count) + " .c files\" " + email); // command for sending email
  popen(email_cmd.c_str(), "w"); // pipe to write command
  cout << "An email has been sent to "<< email << endl;

 return 0;
}
