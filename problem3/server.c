/*
Author: Miranda Dolan
Date: 10/3/2018

FILL IN LATER
*/

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <string>
using namespace std;
int main()
{
    int fd1;

    // FIFO file path
    const char * myfifo = "/tmp/myfifo";
    // Creating the named file(FIFO)
    mkfifo(myfifo, 0666);
    char str[100];
    while (1){
        // First open in read only and read
        fd1 = open(myfifo,O_RDONLY);
        read(fd1, str, 80);
        // Print the read string and close
	string s(str);
        if (s.substr(0,4) == "stop"){
          return 0;
        }else{
          cout<<"Client says: "<<str;
	}
        close(fd1);
    }return 0;
}
