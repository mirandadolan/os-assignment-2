/*
Author: Miranda Dolan
Date: 10/3/2018

FILL IN LATER
*/

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <fstream>
#include <bits/stdc++.h>
#include <vector>
using namespace std;
int main()
{
    int fd;
    string line;
    vector <string> lines;
    ifstream file("myfile.txt");
    string str2;
    while (getline(file, str2)){
      transform(str2.begin(),str2.end(),str2.begin(),toupper);
      str2.push_back('\n');
      lines.push_back(str2);
    }
    lines.push_back("stop");
    char *charArray;
    const char * myfifo = "/tmp/myfifo";

    // Creating the named file(FIFO) mkfifo(<pathname>, <permission>)
    mkfifo(myfifo, 0666);

    char str[100];
    for(int j=0;j<lines.size();j++){
      fd = open(myfifo, O_WRONLY);
      fgets(str, 100, stdin);
      int n = static_cast<int>(lines[j].length());
      charArray= new char[n+1];
      strcpy(charArray,lines[j].c_str());
      write(fd, charArray, n+1);
    }

    close(fd);

    return 0;
}
