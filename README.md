# Operating Systems Assignment 2
Assingment 2 for Operating Systems EECE4029 course at the University of Cincinnati. The assignment consists of 5 different problems listed below.

[Problem 1](#problem-1)  
[Problem 2](#problem-2)  
[Problem 3](#problem-3)  
[Problem 4](#problem-4)  
[Problem 5](#problem-5)  


## Problem 1

The program `hw2p1.cpp` takes two command line parameters. The first one is a program name (linux command); the second one is a file name. If the user enters a wrong number of parameters, display a “usage” message and exit.

The program creates the child process, using `fork()`, and runs the specified program in the child process, using `exec()`. The output is sent to the specified text file using `dup2()`.

To run using `UC Filespace`, execute the following command:
```
clang++ --std=c++11 --stdlib=libc++ h2p1.cpp

./a.out /bin/ps MyResults.txt
```

To run in a standard terminal, execute the following command:
```
g++ h2p1.cpp

./a.out /bin/ps MyResults.txt
```

## Problem 2
The program `hw2p2.cpp` recursively find all the `.c` files under the current directly.

It sends all the file names it found in an email message to an email address provided via the command line.  The subject line contains the number of files found.


To run using `UC Filespace`, execute the following command:
```
 clang++ --std=c++11 --stdlib=libc++ h2p2.cpp

 ./a.out dolanmk@mail.uc.edu
```

To run in a standard terminal, execute the following command:
```
 g++ h2p2.cpp

 ./a.out dolanmk@mail.uc.edu
```

## Problem 3
random text

To run, execute the following command:
```
example
```

## Problem 4
The client program reads a line from `shm-example.txt`, converts everything to upper case letters, and send it to the server, using SysV shared memory. It pauses for 1 second, and repeats the above process, until the end of file.

The client sends a "STOP" string at the end.

To run the server (must be started before client), execute the following command:
```
gcc -o server sharedServer.c

./server &
```

To run the client, execute the following command:
```
gcc -o client sharedClient.c

./client &
```

## Problem 5
random text

To run, execute the following command:
```
example
```
