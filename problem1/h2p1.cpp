/*
Author: Miranda Dolan
Date: 9/26/2018

The program takes two command line parameters. The first one is a program name;
the second one is a file name. The program forks and the child exectutes the
specified program. The output is then written to the specified file instead of
console. See README for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>


int main(int argc, char *argv[]){
  // Check for no value given
  if (argc != 3){
    printf("USAGE ERROR: Please give 2 arguments (program & file name).\n");
    return 0;
  }

  /* Run the child process, execute the program specified, and pipe the output
  to the text file specified.  */
  int pid = fork();
  if (pid == 0) {
      std::string prg = argv[1]; // program
      std::string cmd = prg.substr(5); // make substring for actual command
      std::string output_file = argv[2]; // text file
      printf("Executing your command: %s\n", cmd.c_str());
      printf("Check %s for results!\n", output_file.c_str());
      int filedes = open(output_file.c_str(), O_WRONLY); //create text file for write only
      dup2(filedes, 1); // redirect to text file
      execl(prg.c_str(), cmd.c_str()); // execute program
    }

}
